# ics-ans-role-demo

Ansible role for demo.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-demo
```

## License

BSD 2-clause
