import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_demo_directory(host):
    assert host.file("/opt/demo").is_directory


def test_chrony_service(host):
    service = host.service("chronyd")
    assert service.is_running
    assert service.is_enabled
